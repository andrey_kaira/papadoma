import 'package:flutter/cupertino.dart';

class AppColors{
  static final Color transparent = Color(0x00FFFFFF);
  static final Color white = Color(0xFFFFFFFF);
  static final Color white2 = Color(0xFFF9F9F9);
  static final Color black = Color(0xFF000000);

  static final Color veryDarkGray = Color(0xFF252525);
  static final Color vividOrange = Color(0xFFFFBC0F);
  static final Color orange = Color(0xFFFFA500);
}
import 'package:flutter/cupertino.dart';
import 'package:papa_doma/res/style/app_colors.dart';

class AppShadows {
  static List<BoxShadow> textShadow = [
    BoxShadow(
      offset: const Offset(0.0, 1.0),
      color: AppColors.black.withOpacity(0.4),
      blurRadius: 1.2,
    )
  ];

  static List<BoxShadow> pointsTextShadow = [
    BoxShadow(
      offset: const Offset(0, 0),
      color: AppColors.vividOrange,
      blurRadius: 3.0,
      spreadRadius: 30.0,
    )
  ];


  static List<BoxShadow> primaryShadow({double value, double blurRadius = 10.0, spreadRadius = 5.0}) => [
        BoxShadow(
          color: AppColors.vividOrange.withOpacity(0.4),
          blurRadius: blurRadius * (value ?? 1),
          spreadRadius: spreadRadius * (value ?? 1),
        )
      ];
}

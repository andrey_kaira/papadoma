import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppFonts {
  static TextStyle get montserratLight {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w300,
    );
  }

  static TextStyle get montserratNormal {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w400,
    );
  }

  static TextStyle get montserratMedium {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w500,
    );
  }

  static TextStyle get montserratSemiBold {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w600,
    );
  }

  static TextStyle get montserratBold {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w700,
    );
  }

  static TextStyle get montserratExtraBold {
    return GoogleFonts.montserrat(
      fontWeight: FontWeight.w800,
    );
  }
}

extension TextStyleExtension on TextStyle {
  TextStyle withParams({
    Color color,
    double fontSize,
    double height,
    double letterSpacing,
    TextDecoration decoration,
    double decorationThickness,
    List<BoxShadow> shadows,
  }) {
    return copyWith(
      color: color ?? this.color,
      fontSize: fontSize ?? this.fontSize,
      height: height ?? this.height,
      letterSpacing: letterSpacing ?? this.letterSpacing,
      decoration: decoration,
      decorationThickness: decorationThickness,
      shadows: shadows ?? this.shadows,
    );
  }
}

class AppLocales {
  static final String base = 'he';

  static final String ru = 'ru';
  static final String en = 'en';
  static final String he = 'he';
}

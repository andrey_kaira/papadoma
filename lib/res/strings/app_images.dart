class AppImages {
  static final String cart = 'assets/svg/cart.svg';
  static final String iphone1 = 'assets/png/iphone_1.png';
  static final String iphone2 = 'assets/png/iphone_2.png';
  static final String iphone3 = 'assets/png/iphone_3.png';
  static final String instagram = 'assets/png/instagram.png';
  static final String telegram = 'assets/png/telegram.png';
  static final _StickersImages stickers = _StickersImages();
}

class _StickersImages {
  final String lisa = 'assets/png/lisa.png';
  final String homiak = 'assets/png/homiak.png';
  final String one_home = 'assets/png/one_home.png';
  final String panda = 'assets/png/panda.png';
  final String patrick = 'assets/png/patrick.png';
  final String spanch_bob = 'assets/png/spanch_bob.png';
  final String statuya = 'assets/png/statuya.png';
  final String taison = 'assets/png/taison.png';
  final String tanos = 'assets/png/tanos.png';
  final String uzi = 'assets/png/uzi.png';
  final String yoda_sticker = 'assets/png/yoda_sticker.png';
  final String man = 'assets/png/man.png';
  final String woman = 'assets/png/woman.png';
  final String cat = 'assets/png/cat.png';
}

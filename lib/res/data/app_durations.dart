class AppDurations {
  static const Duration milliseconds200 = const Duration(milliseconds: 200);
  static const Duration milliseconds300 = const Duration(milliseconds: 300);
  static const Duration milliseconds400 = const Duration(milliseconds: 400);

  static const Duration second1 = const Duration(seconds: 1);
  static const Duration second5 = const Duration(seconds: 5);
  static const Duration second40 = const Duration(seconds: 40);
}
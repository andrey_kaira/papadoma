import 'package:flutter/material.dart';

class AppBarDictionary {
  final String topStickers;
  final String aboutBrand;
  final String giveBonus;

  const AppBarDictionary({
    @required this.topStickers,
    @required this.aboutBrand,
    @required this.giveBonus,
  });
}

import 'package:flutter/material.dart';

class MainPageDictionary {
  final String offer;
  final String description;
  final String goToCatalog;
  final String stickerPack;
  final String neededIf;
  final String bigUncle;
  final String stylishGirlfriend;
  final String otherGender;
  final String topSticker;
  final String collectTop;
  final String whyAreWe;
  final String whyAreWePoint1;
  final String whyAreWePoint2;
  final String whyAreWePoint3;
  final String whyAreWePoint4;
  final String feedback;
  final String faq;
  final String question1;
  final String answer1;
  final String question2;
  final String answer2;
  final String question3;
  final String answer3;
  final String question4;
  final String answer4;
  final String giveBonus;
  final String dontsSadCat;
  final String moreFeedback;
  final String telegramText;
  final String instagramSubscribers;
  final String andYou;
  final String goToUp;

  const MainPageDictionary({
    @required this.offer,
    @required this.description,
    @required this.goToCatalog,
    @required this.stickerPack,
    @required this.andYou,
    @required this.neededIf,
    @required this.bigUncle,
    @required this.stylishGirlfriend,
    @required this.otherGender,
    @required this.topSticker,
    @required this.collectTop,
    @required this.whyAreWe,
    @required this.whyAreWePoint1,
    @required this.whyAreWePoint2,
    @required this.whyAreWePoint3,
    @required this.whyAreWePoint4,
    @required this.feedback,
    @required this.moreFeedback,
    @required this.faq,
    @required this.question1,
    @required this.question2,
    @required this.question3,
    @required this.question4,
    @required this.answer1,
    @required this.answer2,
    @required this.answer3,
    @required this.answer4,
    @required this.giveBonus,
    @required this.dontsSadCat,
    @required this.instagramSubscribers,
    @required this.telegramText,
    @required this.goToUp,
  });
}

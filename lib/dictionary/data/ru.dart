import 'package:papa_doma/dictionary/dictionary_classes/app_bar_dictionary.dart';
import 'package:papa_doma/dictionary/dictionary_classes/main_page_dictionary.dart';
import 'package:papa_doma/dictionary/models/language.dart';
import 'package:papa_doma/res/strings/app_locales.dart';

Language ru = Language(
  name: AppLocales.ru,
  appBar: AppBarDictionary(
    topStickers: 'ТОП СТИКЕРОВ',
    aboutBrand: 'О БРЕНДЕ',
    giveBonus: 'ПОЛУЧИТЬ БОНУС',
  ),
  mainPage: MainPageDictionary(
    offer: 'ТРЕНДОВЫЕ СТИКЕРЫ PREMIUM-КАЧЕСТВА',
    description: 'СОБЕРИ СВОЙ СТИКЕРПАК ЗА НЕСКОЛЬКО КЛИКОВ',
    goToCatalog: 'ПЕРЕЙТИ В КАТАЛОГ',
    stickerPack: 'СТИКЕР ПАК',
    neededIf: 'Нужен тебе, если ты:',
    bigUncle: '<b>большущий дядя-мальчик<b>,  который ищет солидные стикеры ',
    stylishGirlfriend: '<b>стильная подружаня<b>, которая\n хочет добавить яркости в жизнь',
    andYou: 'и ты ...',
    otherGender: '<b>Хочешь украсить свою приставку, телефон, ноутбук, IQOS, велик, блокнот или что-либо другое<b>',
    topSticker: 'ТОП СТИКЕРОВ',
    collectTop: 'СОБРАТЬ СВОЙ ТОП',
    whyAreWe: 'ПОЧЕМУ ИМЕННО МЫ?',
    whyAreWePoint1: 'Наш встроенный алгоритм проанализирует <b>твои интересы<b>  и покажет только целевые стикеры',
    whyAreWePoint2: 'Если ты не получишь свой стикерпак  на <b>4й день<b> после заказа мы оплатим доставку',
    whyAreWePoint3: 'Печать на топовой немецком машине гарантирует максимальное <b>качество печати, защиту от влаги и затираний<b>',
    whyAreWePoint4: 'Мы делаем стикеры не только из своих,  но и <b>из твоих изображений<b>',
    feedback: 'ОТЗЫВЫ',
    moreFeedback: 'БОЛЬШЕ ОТЗЫВОВ',
    faq: 'FAQ',
    question1: 'Боятся ли стикеры влаги?',
    question2: 'Можно сделать стикеры из своих фото?',
    question3: 'Есть ли доставка по Украине?',
    question4: 'Какого размера стикеры?',
    answer1: 'Нет. Они покрыты плотной ламинацией и даже если вы разольете на них воду их качетство не ухудшится',
    answer2: 'Да, конечно. Для этого нужно выбрать “СВОЙ СТИКЕР”  в нашем каталоге, после чего вы получите инструкцию',
    answer3: 'Естественно. Отправляем Новой Почтой куда-угодно',
    answer4: 'Размер зависит от формы стикера, обычно это 4-5 см',
    giveBonus: 'ПОЛУЧИТЬ БОНУС',
    dontsSadCat: 'Не расстраивай котика, забирай свой <g>бонус<g>  и переходи в каталог для создания своего\nстикерпака всего за несколько минут',
    instagramSubscribers: 'ПОДПИСАТЬСЯ',
    telegramText: 'НАПИСАТЬ',
    goToUp: 'Вернуться в начало сайта',
  ),
);

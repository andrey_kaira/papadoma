import 'package:flutter/material.dart';
import 'package:papa_doma/dictionary/dictionary_classes/app_bar_dictionary.dart';
import 'package:papa_doma/dictionary/dictionary_classes/main_page_dictionary.dart';

class Language {
  final String name;
  final AppBarDictionary appBar;
  final MainPageDictionary mainPage;

  const Language({
    @required this.name,
    @required this.appBar,
    @required this.mainPage,
  });
}

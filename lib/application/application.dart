import 'package:flutter/material.dart';
import 'package:papa_doma/presentation/pages/main_page/main_page.dart';
import 'package:papa_doma/domain/managers/theme_manager/theme_manager.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeManager.instance.themeData,
      home: MainPage(),
    );
  }
}

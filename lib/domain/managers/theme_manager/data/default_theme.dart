import 'package:flutter/material.dart';
import 'package:papa_doma/res/style/app_colors.dart';

ThemeData defaultTheme = ThemeData.from(
  colorScheme: ColorScheme.dark(
    background: AppColors.veryDarkGray,
    onPrimary: AppColors.vividOrange,
    secondary: AppColors.white,
  ),
);

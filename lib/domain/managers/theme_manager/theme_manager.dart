import 'package:flutter/material.dart';

import 'data/default_theme.dart';

class ThemeManager {
  ThemeData themeData;

  static ThemeManager instance = ThemeManager._();

  ThemeManager._() {
    themeData = defaultTheme;
  }
}

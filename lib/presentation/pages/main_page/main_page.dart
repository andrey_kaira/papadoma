import 'package:flutter/material.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/faq_block/faq_block.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/feedback_block/feedback_block.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/sticker_pack_block/sticker_pack_block.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/top_stickers_block/top_stickers_block.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/welcom_block/welcom_block.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/why_are_we_block/why_are_we_block.dart';
import 'package:papa_doma/presentation/widgets/main_appbar/main_appbar.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Column(
              children: [
                WelcomeBlock(),
                StickerPackBlock(),
                TopStickersBlock(),
                WhyAreWeBlock(),
                FeedbackBlock(),
                FaqBlock(),
              ],
            ),
            MainAppBar(),
          ],
        ),
      ),
    );
  }
}

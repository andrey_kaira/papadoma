import 'package:flutter/material.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/why_are_we_block/widgets/why_are_we_point_item.dart';

class WhyAreWePoints extends StatelessWidget {
  final List<String> points;

  const WhyAreWePoints({
    @required this.points,
  });

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: [
        for (int i = 0; i < points.length; i++)
          Center(child: WhyAreWePointItem(position: i + 1, point: points[i])),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:papa_doma/presentation/widgets/main_text_block/main_text_block.dart';
import 'package:papa_doma/res/data/app_durations.dart';
import 'package:papa_doma/res/style/app_colors.dart';
import 'package:papa_doma/res/style/app_fonts.dart';
import 'package:papa_doma/res/style/app_shadows.dart';

class WhyAreWePointItem extends StatefulWidget {
  final int position;
  final String point;

  const WhyAreWePointItem({
    @required this.position,
    @required this.point,
  });

  @override
  _WhyAreWePointItemState createState() => _WhyAreWePointItemState();
}

class _WhyAreWePointItemState extends State<WhyAreWePointItem> with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(vsync: this, duration: AppDurations.second1);
    Future.delayed(AppDurations.second1).then((value) => _controller.forward());
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (BuildContext context, Widget _) {
        return Opacity(
          opacity: _controller.value,
          child: Stack(
            children: [
              Text(
                widget.position.toString(),
                style: AppFonts.montserratExtraBold.withParams(
                  color: AppColors.veryDarkGray,
                  shadows: AppShadows.pointsTextShadow,
                  fontSize: 180.0,
                ),
              ),
              Column(
                children: [
                  const SizedBox(height: 140.0),
                  MainTextBlock(content: widget.point, width: MediaQuery.of(context).size.width * 0.35, fontSize: 26.0, showShadow: true),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}

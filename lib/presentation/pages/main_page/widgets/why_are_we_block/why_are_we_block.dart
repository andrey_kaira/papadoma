import 'package:flutter/material.dart';
import 'package:papa_doma/dictionary/dictionary_classes/main_page_dictionary.dart';
import 'package:papa_doma/dictionary/flutter_dictionary.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/why_are_we_block/widgets/why_are_we_points.dart';
import 'package:papa_doma/presentation/widgets/title_block/title_block.dart';

class WhyAreWeBlock extends StatelessWidget {
  final MainPageDictionary dictionary = FlutterDictionary.instance.language.mainPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TitleBlock(title: dictionary.whyAreWe),
        const SizedBox(height: 60.0),
        WhyAreWePoints(
          points: [
            dictionary.whyAreWePoint1,
            dictionary.whyAreWePoint2,
            dictionary.whyAreWePoint3,
            dictionary.whyAreWePoint4,
          ],
        ),
        const SizedBox(height: 60.0),
      ],
    );
  }
}

import 'package:flutter/material.dart';

import 'dart:math' as math;

import 'package:papa_doma/res/style/app_colors.dart';

class BackgroundWelcomeBlock extends StatefulWidget {
  final double height;
  final double width;

  const BackgroundWelcomeBlock({
    this.height = 800.0,
    this.width = 420.0,
  });

  @override
  _BackgroundWelcomeBlockState createState() => _BackgroundWelcomeBlockState();
}

class _BackgroundWelcomeBlockState extends State<BackgroundWelcomeBlock> with SingleTickerProviderStateMixin {
  double waveRadius = 0.0;
  double waveGap = 30.0;
  Animation<double> _animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(duration: const Duration(milliseconds: 4000), vsync: this);

    controller.repeat(reverse: true);
  }

  @override
  Widget build(BuildContext context) {
    _animation = Tween(begin: 0.0, end: waveGap).animate(controller)
      ..addListener(() {
        setState(() {
          waveRadius = _animation.value;
        });
      });

    return CustomPaint(
      size: Size(widget.width, widget.height),
      painter: CircleWavePainter(waveRadius),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class CircleWavePainter extends CustomPainter {
  final double waveRadius;
  var wavePaint;

  CircleWavePainter(this.waveRadius) {
    wavePaint = Paint()
      ..color = AppColors.vividOrange.withOpacity(0.15)
      ..style = PaintingStyle.stroke
      ..shader
      ..strokeWidth = 2.0
      ..isAntiAlias = true;
  }

  @override
  void paint(Canvas canvas, Size size) {
    double centerX = size.width / 2.0;
    double centerY = size.height / 2.0;
    double maxRadius = hypot(centerX, centerY);

    var currentRadius = waveRadius;
    while (currentRadius  + (centerY / 10.0) < maxRadius) {
      canvas.drawCircle(Offset(centerX, centerY), currentRadius, wavePaint);
      currentRadius += centerY / 3.0;
    }
  }

  @override
  bool shouldRepaint(CircleWavePainter oldDelegate) {
    return oldDelegate.waveRadius != waveRadius;
  }

  double hypot(double x, double y) {
    return math.sqrt(x * x + y * y);
  }
}

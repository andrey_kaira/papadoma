import 'package:flutter/material.dart';
import 'package:papa_doma/res/data/app_durations.dart';
import 'package:papa_doma/res/strings/app_default_string.dart';
import 'package:papa_doma/res/style/app_colors.dart';
import 'package:papa_doma/res/style/app_fonts.dart';
import 'package:papa_doma/res/style/app_shadows.dart';

class AnimatedTextBlock extends StatefulWidget {
  final bool isAnimation;

  const AnimatedTextBlock({
    this.isAnimation = true,
  });

  @override
  _AnimatedTextBlockState createState() => _AnimatedTextBlockState();
}

class _AnimatedTextBlockState extends State<AnimatedTextBlock> with SingleTickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();
  AnimationController _animationController;
  Animation<double> _animationPosition;

  @override
  void initState() {
    _animationController = AnimationController(vsync: this, duration: AppDurations.second40);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _animationPosition = Tween<double>(begin: MediaQuery.of(context).size.width, end: 0).animate(_animationController);
      _animationController.addListener(() {
        _scrollController.jumpTo(_animationPosition.value);
      });
    });
    if(widget.isAnimation) {
      _animationController.repeat();
    }
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.vividOrange,
        boxShadow: AppShadows.textShadow,
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        controller: _scrollController,
        physics: const NeverScrollableScrollPhysics(),
        child: Row(
          children: [
            for (int i = 0; i < 80; i++)
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: SelectableText(
                  AppDefaultString.companyName,
                  style: AppFonts.montserratSemiBold.withParams(
                    shadows: AppShadows.textShadow,
                    fontSize: 18.0,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:papa_doma/dictionary/dictionary_classes/main_page_dictionary.dart';
import 'package:papa_doma/dictionary/flutter_dictionary.dart';
import 'package:papa_doma/presentation/widgets/main_button/main_button.dart';
import 'package:papa_doma/res/style/app_fonts.dart';

class ContentWelcomeBlock extends StatelessWidget {
  final MainPageDictionary mainPageDictionary = FlutterDictionary.instance.language.mainPage;

  @override
  Widget build(BuildContext context) {
    return Center(
      heightFactor: 0.75,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AutoSizeText(
            mainPageDictionary.offer,
            style: AppFonts.montserratExtraBold.withParams(fontSize: 50.0, letterSpacing: 1.5),
            textAlign: TextAlign.center,
            maxLines: 1,
          ),
          AutoSizeText(
            mainPageDictionary.description,
            style: AppFonts.montserratMedium.withParams(fontSize: 30.0, letterSpacing: 1.5),
            textAlign: TextAlign.center,
            maxLines: 1,
          ),
          const SizedBox(height: 48.0),
          MainButton(title: mainPageDictionary.goToCatalog, onTap: (){}),
        ],
      ),
    );
  }
}

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/welcom_block/widgets/stickers_footer_stack.dart';
import 'package:papa_doma/res/style/app_shadows.dart';

import 'animated_text_block.dart';

class FooterWelcomeBlock extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: double.infinity,
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            height: 50.0,
            width: double.infinity,
            margin: EdgeInsets.only(bottom: 30),
            decoration: BoxDecoration(
              boxShadow: AppShadows.primaryShadow(blurRadius: 30.0, spreadRadius: 50.0),
            ),
          ),
          StickersFooterStack(),
          AnimatedTextBlock(),
          Transform.rotate(
            angle: pi*0.015,
            child: AnimatedTextBlock(isAnimation: false),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:papa_doma/presentation/widgets/sticker_item/sticker_item.dart';
import 'package:papa_doma/res/strings/app_images.dart';

class StickersFooterStack extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.bottomCenter,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            StickerItem(
              image: AppImages.stickers.patrick,
              size: 220,
            ),
            StickerItem(
              image: AppImages.stickers.panda,
              size: 200,
            ),
            StickerItem(
              image: AppImages.stickers.lisa,
              size: 220,
            ),
            StickerItem(
              image: AppImages.stickers.tanos,
              size: 220,
            ),
            StickerItem(
              image: AppImages.stickers.homiak,
              size: 220,
            ),
            StickerItem(
              image: AppImages.stickers.yoda_sticker,
              size: 220,
            ),
            StickerItem(
              image: AppImages.stickers.taison,
              size: 220,
            ),
            StickerItem(
              image: AppImages.stickers.uzi,
              size: 200,
            ),
            StickerItem(
              image: AppImages.stickers.spanch_bob,
              size: 220,
            ),
            StickerItem(
              image: AppImages.stickers.one_home,
              size: 220,
            ),
            StickerItem(
              image: AppImages.stickers.statuya,
              size: 220,
            ),
          ],
        ),
      ),
    );
  }
}

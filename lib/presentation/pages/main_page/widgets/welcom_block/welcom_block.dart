import 'package:flutter/material.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/welcom_block/widgets/background_welcom_block.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/welcom_block/widgets/content_welcom_block.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/welcom_block/widgets/footer_welcome_block.dart';

class WelcomeBlock extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 620.0,
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          BackgroundWelcomeBlock(),
          ContentWelcomeBlock(),
          FooterWelcomeBlock(),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:papa_doma/dictionary/dictionary_classes/main_page_dictionary.dart';
import 'package:papa_doma/dictionary/flutter_dictionary.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/sticker_pack_block/widgets/sticker_gender_block.dart';
import 'package:papa_doma/presentation/widgets/main_text_block/main_text_block.dart';
import 'package:papa_doma/presentation/widgets/title_block/title_block.dart';
import 'package:papa_doma/res/style/app_colors.dart';
import 'package:papa_doma/res/style/app_fonts.dart';

class StickerPackBlock extends StatelessWidget {
  final MainPageDictionary dictionary = FlutterDictionary.instance.language.mainPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TitleBlock(title: dictionary.stickerPack, description: dictionary.neededIf),
        const SizedBox(height: 60.0),
        StickerGenderBlock(),
        SelectableText(
          dictionary.andYou,
          style: AppFonts.montserratLight.withParams(fontSize: 40.0, color: AppColors.white2),
        ),
        const SizedBox(height: 20.0),
        MainTextBlock(content: dictionary.otherGender, showShadow: true, fontSize: 20.0),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:papa_doma/dictionary/dictionary_classes/main_page_dictionary.dart';
import 'package:papa_doma/dictionary/flutter_dictionary.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/sticker_pack_block/widgets/sticker_gender_item.dart';
import 'package:papa_doma/res/data/app_values.dart';
import 'package:papa_doma/res/strings/app_images.dart';

class StickerGenderBlock extends StatelessWidget {
  final MainPageDictionary dictionary = FlutterDictionary.instance.language.mainPage;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width > AppValues.smallSize ? 400.0 : null,
      child: Center(
        child: ListView(
          shrinkWrap: true,
          scrollDirection: MediaQuery.of(context).size.width < AppValues.smallSize ? Axis.vertical : Axis.horizontal,
          children: [
            StickerGenderItem(
              image: AppImages.stickers.man,
              content: dictionary.bigUncle,
            ),
            SizedBox(width: MediaQuery.of(context).size.width * 0.1, height: MediaQuery.of(context).size.width * 0.1),
            StickerGenderItem(
              image: AppImages.stickers.woman,
              content: dictionary.stylishGirlfriend,
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/welcom_block/widgets/background_welcom_block.dart';
import 'package:papa_doma/presentation/widgets/main_text_block/main_text_block.dart';
import 'package:papa_doma/presentation/widgets/sticker_item/sticker_item.dart';
import 'package:papa_doma/res/style/app_colors.dart';
import 'package:papa_doma/res/style/app_fonts.dart';
import 'package:papa_doma/res/style/app_shadows.dart';

class StickerGenderItem extends StatelessWidget {
  final String image;
  final String content;

  const StickerGenderItem({
    @required this.image,
    @required this.content,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 345.0,
      child: Stack(
        alignment: Alignment.center,
        children: [
          BackgroundWelcomeBlock(
            width: 300.0,
            height: 300.0,
          ),
          Container(
            height: 30.0,
            width: 30.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: AppShadows.primaryShadow(blurRadius: 30, spreadRadius: 80),
            ),
          ),
          StickerItem(image: image, size: 300.0),
          Align(
            alignment: const Alignment(0.0, 1.0),
            child: MainTextBlock(content: content, width: 10.0),
          ),
        ],
      ),
    );
  }
}

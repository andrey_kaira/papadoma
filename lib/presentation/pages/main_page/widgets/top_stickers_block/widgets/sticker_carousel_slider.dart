import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:papa_doma/presentation/widgets/sticker_item/sticker_item.dart';
import 'package:papa_doma/res/data/app_durations.dart';
import 'package:papa_doma/res/style/app_shadows.dart';

class StickerCarouselSlider extends StatelessWidget {
  final List<String> images;

  const StickerCarouselSlider({
    @required this.images,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 260.0,
      child: CarouselSlider(
        items: images.map((String image) {
          return SizedBox.expand(
            child: Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  height: 30.0,
                  width: 30.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    boxShadow: AppShadows.primaryShadow(blurRadius: 20.0, spreadRadius: 80.0),
                  ),
                ),
                StickerItem(image: image, size: 200.0),
              ],
            ),
          );
        }).toList(),
        options: CarouselOptions(
          pageSnapping: false,
          viewportFraction: (1080 / MediaQuery.of(context).size.width) / 4,
          autoPlay: true,
          autoPlayAnimationDuration: AppDurations.second1,
          autoPlayInterval: AppDurations.second5,
        ),
      ),
    );
  }
}

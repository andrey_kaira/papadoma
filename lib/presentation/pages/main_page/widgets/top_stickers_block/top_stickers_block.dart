import 'package:flutter/material.dart';
import 'package:papa_doma/dictionary/dictionary_classes/main_page_dictionary.dart';
import 'package:papa_doma/dictionary/flutter_dictionary.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/top_stickers_block/widgets/sticker_carousel_slider.dart';
import 'package:papa_doma/presentation/widgets/main_button/main_button.dart';
import 'package:papa_doma/presentation/widgets/title_block/title_block.dart';
import 'package:papa_doma/res/strings/app_images.dart';

class TopStickersBlock extends StatelessWidget {
  final MainPageDictionary dictionary = FlutterDictionary.instance.language.mainPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TitleBlock(title: dictionary.topSticker),
        const SizedBox(height: 60.0),
        StickerCarouselSlider(
          images: [
            AppImages.stickers.woman,
            AppImages.stickers.man,
            AppImages.stickers.statuya,
            AppImages.stickers.taison,
            AppImages.stickers.patrick,
            AppImages.stickers.spanch_bob,
            AppImages.stickers.uzi,
            AppImages.stickers.lisa,
            AppImages.stickers.tanos,
            AppImages.stickers.homiak,
            AppImages.stickers.panda,
            AppImages.stickers.one_home,
          ],
        ),
        const SizedBox(height: 40.0),
        MainButton(title: dictionary.collectTop, onTap: (){}),
      ],
    );
  }
}

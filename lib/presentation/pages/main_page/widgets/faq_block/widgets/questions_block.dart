import 'package:flutter/material.dart';
import 'package:papa_doma/dictionary/dictionary_classes/main_page_dictionary.dart';
import 'package:papa_doma/dictionary/flutter_dictionary.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/faq_block/widgets/question_item.dart';

class QuestionsBlock extends StatelessWidget {
  final MainPageDictionary dictionary = FlutterDictionary.instance.language.mainPage;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        QuestionItem(question: dictionary.question1, answer: dictionary.answer1),
        QuestionItem(question: dictionary.question2, answer: dictionary.answer2),
        QuestionItem(question: dictionary.question3, answer: dictionary.answer3),
        QuestionItem(question: dictionary.question4, answer: dictionary.answer4),
      ],
    );
  }
}

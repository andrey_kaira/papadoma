import 'package:flutter/material.dart';
import 'package:papa_doma/res/style/app_colors.dart';
import 'package:papa_doma/res/style/app_fonts.dart';

class OtherAppButton extends StatelessWidget {
  final String image;
  final String title;

  const OtherAppButton({
    @required this.image,
    @required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.centerLeft,
      children: [
        Container(
          margin: const EdgeInsets.only(left: 52.0),
          decoration: BoxDecoration(
            color: AppColors.white,
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: InkWell(
            onTap: () {},
            borderRadius: BorderRadius.circular(8.0),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 16.0),

              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: Text(
                title,
                style: AppFonts.montserratSemiBold.withParams(fontSize: 18.0, color: AppColors.black),
              ),
            ),
          ),
        ),
        Image.asset(image, height: 68.0, width: 68.0),
      ],
    );
  }
}

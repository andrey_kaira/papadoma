import 'package:flutter/material.dart';
import 'package:papa_doma/dictionary/flutter_dictionary.dart';
import 'package:papa_doma/presentation/widgets/main_text_block/main_text_block.dart';
import 'package:papa_doma/presentation/widgets/sticker_item/sticker_item.dart';
import 'package:papa_doma/res/strings/app_images.dart';
import 'package:papa_doma/res/style/app_shadows.dart';

class SadCatBlock extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 220.0,
      child: Stack(
        alignment: Alignment.bottomRight,
        children: [
          SizedBox(
            child: MainTextBlock(
              content: FlutterDictionary.instance.language.mainPage.dontsSadCat,
              showShadow: true,
              fontSize: 24.0,
              textAlign: TextAlign.start,
              width: MediaQuery.of(context).size.width * 0.35,
              pading: 40.0,
            ),
          ),
          Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: 30.0,
                width: 30.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  boxShadow: AppShadows.primaryShadow(blurRadius: 30, spreadRadius: 60),
                ),
              ),
              StickerItem(image: AppImages.stickers.cat, size: 180),
            ],
          ),
        ],
      ),
    );
  }
}

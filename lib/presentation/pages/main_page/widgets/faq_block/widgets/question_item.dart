import 'package:flutter/material.dart';
import 'package:papa_doma/res/style/app_colors.dart';
import 'package:papa_doma/res/style/app_fonts.dart';

class QuestionItem extends StatelessWidget {
  final String question;
  final String answer;

  const QuestionItem({
    @required this.question,
    @required this.answer,
  });

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width * 0.3;
    return Container(
      width: width > 400.0 ? width : 400.0,
      decoration: BoxDecoration(color: AppColors.white, borderRadius: BorderRadius.circular(24.0)),
      padding: const EdgeInsets.all(16.0),
      margin: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            question,
            style: AppFonts.montserratBold.withParams(fontSize: 24.0, color: AppColors.black),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 12.0),
          Text(
            answer,
            style: AppFonts.montserratNormal.withParams(fontSize: 18.0, color: AppColors.black),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}

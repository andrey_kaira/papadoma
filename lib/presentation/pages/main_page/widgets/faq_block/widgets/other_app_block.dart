import 'package:flutter/material.dart';
import 'package:papa_doma/dictionary/dictionary_classes/main_page_dictionary.dart';
import 'package:papa_doma/dictionary/flutter_dictionary.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/faq_block/widgets/other_app_button.dart';
import 'package:papa_doma/res/strings/app_images.dart';

class OtherAppBlock extends StatelessWidget {
  final MainPageDictionary dictionary = FlutterDictionary.instance.language.mainPage;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        OtherAppButton(image: AppImages.telegram, title: dictionary.telegramText),
        SizedBox(width: MediaQuery.of(context).size.width * 0.15),
        OtherAppButton(image: AppImages.instagram, title: dictionary.instagramSubscribers),
      ],
    );
  }
}

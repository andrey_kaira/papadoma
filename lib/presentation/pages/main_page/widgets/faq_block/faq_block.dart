import 'package:flutter/material.dart';
import 'package:papa_doma/dictionary/dictionary_classes/main_page_dictionary.dart';
import 'package:papa_doma/dictionary/flutter_dictionary.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/faq_block/widgets/other_app_block.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/faq_block/widgets/questions_block.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/faq_block/widgets/sad_cat_block.dart';
import 'package:papa_doma/presentation/widgets/main_button/main_button.dart';
import 'package:papa_doma/presentation/widgets/title_block/title_block.dart';

class FaqBlock extends StatelessWidget {
  final MainPageDictionary dictionary = FlutterDictionary.instance.language.mainPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TitleBlock(title: dictionary.faq),
        const SizedBox(height: 30.0),
        QuestionsBlock(),
        const SizedBox(height: 60.0),
        SadCatBlock(),
        const SizedBox(height: 30.0),
        MainButton(
          title: dictionary.giveBonus,
          onTap: () {},
          height: 35,
          width: 300,
          fontSize: 24.0,
        ),
        const SizedBox(height: 40.0),
        OtherAppBlock(),
        const SizedBox(height: 40.0),
      ],
    );
  }
}

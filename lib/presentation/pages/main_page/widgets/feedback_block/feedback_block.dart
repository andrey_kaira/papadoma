import 'package:flutter/material.dart';
import 'package:papa_doma/dictionary/dictionary_classes/main_page_dictionary.dart';
import 'package:papa_doma/dictionary/flutter_dictionary.dart';
import 'package:papa_doma/presentation/pages/main_page/widgets/feedback_block/widgets/more_feedback.dart';
import 'package:papa_doma/presentation/widgets/title_block/title_block.dart';

class FeedbackBlock extends StatelessWidget {
  final MainPageDictionary dictionary = FlutterDictionary.instance.language.mainPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TitleBlock(title: dictionary.feedback),
        const SizedBox(height: 60.0),
        MoreFeedback(),
        const SizedBox(height: 30.0),
      ],
    );
  }
}

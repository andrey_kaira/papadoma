import 'package:flutter/material.dart';
import 'package:papa_doma/dictionary/flutter_dictionary.dart';
import 'package:papa_doma/presentation/widgets/main_button/main_button.dart';
import 'package:papa_doma/presentation/widgets/sticker_item/sticker_item.dart';
import 'package:papa_doma/res/strings/app_images.dart';
import 'package:papa_doma/res/style/app_shadows.dart';

class MoreFeedback extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 500,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            height: 30.0,
            width: 30.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: AppShadows.primaryShadow(blurRadius: 50, spreadRadius: 180),
            ),
          ),
          Align(alignment: Alignment(-0.15, 0), child: StickerItem(image: AppImages.iphone3, size: 400)),
          Align(alignment: Alignment(0.15, 0), child: StickerItem(image: AppImages.iphone2, size: 400)),
          StickerItem(image: AppImages.iphone1, size: 400),
          Align(
            alignment: Alignment.bottomCenter,
            child: MainButton(
              title: FlutterDictionary.instance.language.mainPage.moreFeedback,
              onTap: (){},
            ),
          ),
        ],
      ),
    );
  }
}

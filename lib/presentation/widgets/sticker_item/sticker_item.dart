import 'package:flutter/material.dart';
import 'package:papa_doma/res/data/app_durations.dart';
import 'package:papa_doma/res/style/app_colors.dart';

class StickerItem extends StatefulWidget {
  final String image;
  double size;

  StickerItem({
    @required this.image,
    this.size = 300,
  }) {
    if (size < 160) {
      size = 160;
    }
  }

  @override
  _StickerItemState createState() => _StickerItemState();
}

class _StickerItemState extends State<StickerItem> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _animationSize;

  @override
  void initState() {
    _controller = AnimationController(vsync: this, duration: AppDurations.milliseconds400);
    _animationSize = Tween<double>(begin: 0.0, end: 20.0).animate(_controller);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (BuildContext context, Widget _) {
        return InkWell(
          hoverColor: AppColors.transparent,
          splashColor: AppColors.transparent,
          highlightColor: AppColors.transparent,
          onHover: _onHover,
          onTap: () {},
          child: Image.asset(
            widget.image,
            height: widget.size + _animationSize.value,
          ),
        );
      },
    );
  }

  void _onHover(bool isFocus) {
    if (isFocus) {
      _controller.forward();
    } else {
      _controller.reverse();
    }
  }
}

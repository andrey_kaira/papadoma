import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:papa_doma/res/style/app_colors.dart';
import 'package:papa_doma/res/style/app_fonts.dart';

class TitleBlock extends StatelessWidget {
  final String title;
  final String description;

  const TitleBlock({
    @required this.title,
    this.description,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 40.0),
        Stack(
          alignment: Alignment.bottomCenter,
          children: [
            AutoSizeText(
              title,
              style: AppFonts.montserratBold.withParams(
                fontSize: 80.0,
                color: AppColors.white.withOpacity(0.05),
              ),
              textAlign: TextAlign.center,
              maxLines: 1,
            ),
            AutoSizeText(
              title,
              style: AppFonts.montserratBold.withParams(
                fontSize: 60.0,
                color: AppColors.vividOrange,
              ),
              textAlign: TextAlign.center,
              maxLines: 1,
            ),
          ],
        ),
        if (description != null)
          SelectableText(
            description,
            style: AppFonts.montserratLight.withParams(
              fontSize: 40.0,
              color: AppColors.white2,
            ),
          ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:papa_doma/res/data/app_durations.dart';
import 'package:papa_doma/res/style/app_colors.dart';
import 'package:papa_doma/res/style/app_fonts.dart';
import 'package:papa_doma/res/style/app_shadows.dart';

class MainButton extends StatefulWidget {
  final String title;
  final double width;
  final double height;
  final double fontSize;
  final void Function() onTap;

  const MainButton({
    @required this.title,
    @required this.onTap,
    this.width = 360.0,
    this.height = 55.0,
    this.fontSize = 30.0,
  });

  @override
  _MainButtonState createState() => _MainButtonState();
}

class _MainButtonState extends State<MainButton> with TickerProviderStateMixin {
  AnimationController _colorController;
  AnimationController _sizeController;

  Animation<Color> colorAnimation;
  Animation<double> sizeAnimation;

  @override
  void initState() {
    _colorController = AnimationController(vsync: this, duration: AppDurations.milliseconds400);
    _sizeController = AnimationController(vsync: this, duration: AppDurations.milliseconds300);

    colorAnimation = ColorTween(begin: AppColors.vividOrange, end: AppColors.orange).animate(_colorController);
    sizeAnimation = Tween<double>(begin: 0.0, end: 8.0).animate(_sizeController);

    _colorController.addListener(updateState);
    _sizeController.addListener(updateState);
    _sizeController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _sizeController.reverse();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _colorController.removeListener(updateState);
    _sizeController.removeListener(updateState);
    _colorController.dispose();
    _sizeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.height + 12.0,
      width: widget.width + 12.0,
      child: InkWell(
        hoverColor: AppColors.transparent,
        splashColor: AppColors.transparent,
        highlightColor: AppColors.transparent,
        onHover: _onHover,
        onTap: () {
          _sizeController.forward();
          widget.onTap();
        },
        child: AnimatedContainer(
          decoration: BoxDecoration(
            color: colorAnimation.value,
            borderRadius: BorderRadius.circular(16.0),
            boxShadow: AppShadows.primaryShadow(value: 1 - _sizeController.value),
          ),
          duration: AppDurations.milliseconds400,
          margin: EdgeInsets.all(sizeAnimation.value),
          alignment: Alignment.center,
          child: Text(
            widget.title,
            style: AppFonts.montserratMedium.withParams(fontSize: widget.fontSize, shadows: AppShadows.textShadow),
          ),
        ),
      ),
    );
  }

  void _onHover(bool isFocus) {
    if (isFocus) {
      _colorController.forward();
    } else {
      _colorController.reverse();
    }
  }

  void Function() get updateState => () => setState(() {});
}

import 'package:flutter/material.dart';
import 'package:papa_doma/res/style/app_colors.dart';
import 'package:papa_doma/res/style/app_fonts.dart';
import 'package:papa_doma/res/style/app_shadows.dart';

class MainTextBlock extends StatelessWidget {
  final String content;
  final bool showShadow;
  final double fontSize;
  final double width;
  final double pading;
  final TextAlign textAlign;

  const MainTextBlock({
    @required this.content,
    this.showShadow = false,
    this.fontSize = 16.0,
    this.pading = 0.0,
    this.width,
    this.textAlign = TextAlign.center,
  });

  @override
  Widget build(BuildContext context) {
    final List<String> data = content.replaceAll('<g>', '<b>').split('<b>');
    data.removeWhere((element) => element == '' || element == ' ');
    final double widthBlock = MediaQuery.of(context).size.width * 0.3;
    return Container(
      width: (width ?? widthBlock) > 400.0 ? (width ?? widthBlock) : 400.0,
      decoration: BoxDecoration(
        color: AppColors.white,
        boxShadow: showShadow ? AppShadows.primaryShadow() : AppShadows.textShadow,
        borderRadius: BorderRadius.circular(16.0),
      ),
      padding: const EdgeInsets.all(16.0),
      child: Padding(
        padding: EdgeInsets.only(right: pading),
        child: RichText(
          textAlign: textAlign,
          text: TextSpan(
            children: <TextSpan>[
              for (String str in data)
                if (content
                        .substring((content.indexOf(str) - 3 > 0 ? content.indexOf(str) - 3 : 0),
                            str.length + content.indexOf(str) + 3 > content.length ? content.length : str.length + content.indexOf(str) + 3)
                        .startsWith('<b>') &&
                    content
                        .substring(content.indexOf(str),
                            str.length + content.indexOf(str) + 3 > content.length ? content.length : str.length + content.indexOf(str) + 3)
                        .endsWith('<b>'))
                  TextSpan(
                    text: str,
                    style: AppFonts.montserratSemiBold.withParams(color: AppColors.black, fontSize: fontSize),
                  )
                else if (content
                        .substring((content.indexOf(str) - 3 > 0 ? content.indexOf(str) - 3 : 0),
                            str.length + content.indexOf(str) + 3 > content.length ? content.length : str.length + content.indexOf(str) + 3)
                        .startsWith('<g>') &&
                    content
                        .substring(content.indexOf(str),
                            str.length + content.indexOf(str) + 3 > content.length ? content.length : str.length + content.indexOf(str) + 3)
                        .endsWith('<g>'))
                  TextSpan(
                    text: str,
                    style: AppFonts.montserratSemiBold.withParams(color: AppColors.vividOrange, fontSize: fontSize),
                  )
                else
                  TextSpan(
                    text: str,
                    style: AppFonts.montserratNormal.withParams(color: AppColors.black, fontSize: fontSize),
                  ),
            ],
          ),
        ),
      ),
    );
  }
}

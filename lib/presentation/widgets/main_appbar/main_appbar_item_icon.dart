import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:papa_doma/res/data/app_durations.dart';
import 'package:papa_doma/res/strings/app_images.dart';
import 'package:papa_doma/res/style/app_colors.dart';

class MainAppBarItemIcon extends StatefulWidget {
  final void Function() onTap;
  final double fontSize;

  const MainAppBarItemIcon({
    @required this.onTap,
    this.fontSize = 50.0,
  });

  @override
  _MainAppBarItemIconState createState() => _MainAppBarItemIconState();
}

class _MainAppBarItemIconState extends State<MainAppBarItemIcon> with TickerProviderStateMixin {
  AnimationController _colorController;
  AnimationController _sizeController;

  Animation<Color> colorAnimation;
  Animation<double> sizeAnimation;

  @override
  void initState() {
    _colorController = AnimationController(vsync: this, duration: AppDurations.milliseconds400);
    _sizeController = AnimationController(vsync: this, duration: AppDurations.milliseconds300);

    colorAnimation = ColorTween(begin: AppColors.white, end: AppColors.vividOrange).animate(_colorController);
    sizeAnimation = Tween<double>(begin: widget.fontSize, end: widget.fontSize + 8).animate(_sizeController);

    _colorController.addListener(updateState);
    _sizeController.addListener(updateState);
    _sizeController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _sizeController.reverse();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _colorController.removeListener(updateState);
    _sizeController.removeListener(updateState);
    _colorController.dispose();
    _sizeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Center(
        child: InkWell(
          hoverColor: AppColors.transparent,
          splashColor: AppColors.transparent,
          highlightColor: AppColors.transparent,
          onHover: _onHover,
          onTap: () {
            _sizeController.forward();
            widget.onTap();
          },
          child: SvgPicture.asset(AppImages.cart, height: sizeAnimation.value, color: colorAnimation.value),
        ),
      ),
    );
  }

  void _onHover(bool isFocus) {
    if (isFocus) {
      _colorController.forward();
    } else {
      _colorController.reverse();
    }
  }

  void Function() get updateState => () => setState(() {});
}

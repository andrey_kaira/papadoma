import 'package:flutter/material.dart';
import 'package:papa_doma/dictionary/dictionary_classes/app_bar_dictionary.dart';
import 'package:papa_doma/dictionary/flutter_dictionary.dart';
import 'package:papa_doma/presentation/widgets/main_appbar/main_appbar_item_icon.dart';
import 'package:papa_doma/presentation/widgets/main_appbar/main_appbar_item_text.dart';
import 'package:papa_doma/res/strings/app_default_string.dart';

class MainAppBar extends StatelessWidget implements PreferredSizeWidget {
  final double height;
  final AppBarDictionary dictionary = FlutterDictionary.instance.language.appBar;

  MainAppBar({
    Key key,
    @required this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: preferredSize.height,
      child: Row(
        children: [
          MainAppBarItemText(
            onTap: () {},
            title: AppDefaultString.companyName,
            fontSize: 30.0,
          ),
          MainAppBarItemText(
            onTap: () {},
            title: dictionary.topStickers,
          ),
          MainAppBarItemText(
            onTap: () {},
            title: dictionary.aboutBrand,
          ),
          MainAppBarItemText(
            onTap: () {},
            title: dictionary.giveBonus,
          ),
          MainAppBarItemIcon(onTap: () {}),
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(48.0 * 2);
}

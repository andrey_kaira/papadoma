import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:papa_doma/res/data/app_durations.dart';
import 'package:papa_doma/res/style/app_colors.dart';
import 'package:papa_doma/res/style/app_fonts.dart';

class MainAppBarItemText extends StatefulWidget {
  final String title;
  final void Function() onTap;
  final double fontSize;

  const MainAppBarItemText({
    @required this.title,
    @required this.onTap,
    this.fontSize = 20.0,
  });

  @override
  _MainAppBarItemTextState createState() => _MainAppBarItemTextState();
}

class _MainAppBarItemTextState extends State<MainAppBarItemText> with TickerProviderStateMixin {
  AnimationController _colorController;
  AnimationController _sizeController;

  Animation<Color> colorAnimation;
  Animation<double> sizeAnimation;

  @override
  void initState() {
    _colorController = AnimationController(vsync: this, duration: AppDurations.milliseconds400);
    _sizeController = AnimationController(vsync: this, duration: AppDurations.milliseconds300);

    colorAnimation = ColorTween(begin: AppColors.white, end: AppColors.vividOrange).animate(_colorController);
    sizeAnimation = Tween<double>(begin: widget.fontSize, end: widget.fontSize + 4).animate(_sizeController);

    _colorController.addListener(updateState);
    _sizeController.addListener(updateState);
    _sizeController.addStatusListener((status) {
      if(status == AnimationStatus.completed){
        _sizeController.reverse();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _colorController.removeListener(updateState);
    _sizeController.removeListener(updateState);
    _colorController.dispose();
    _sizeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        hoverColor: AppColors.transparent,
        splashColor: AppColors.transparent,
        highlightColor: AppColors.transparent,
        onHover: _onHover,
        onTap: (){
          _sizeController.forward();
          widget.onTap();
        },
        child: AutoSizeText(
          widget.title,
          style: AppFonts.montserratBold.withParams(fontSize: sizeAnimation.value, color: colorAnimation.value),
          textAlign: TextAlign.center,
          maxLines: 1,
        ),
      ),
    );
  }

  void _onHover(bool isFocus) {
    if (isFocus) {
      _colorController.forward();
    } else {
      _colorController.reverse();
    }
  }

  void Function() get updateState => () => setState(() {});
}
